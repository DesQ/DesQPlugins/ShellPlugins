/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>
#include <DFL/DF5/IpcClient.hpp>

#include "Menu.hpp"

MenuPluginWidget::MenuPluginWidget( QWidget *parent ) : DesQ::Panel::PluginWidget( parent ) {
    menu = new QLabel();

    menu->setAlignment( Qt::AlignCenter );
    menu->setPixmap( QIcon::fromTheme( "desq" ).pixmap( panelHeight - 4 ) );
    menu->setAlignment( Qt::AlignCenter );
    menu->setSizePolicy( QSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred ) );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins() );
    lyt->addWidget( menu );
    setLayout( lyt );

    mTooltip = new QLabel( "Click to show DesQ Menu" );

    mTooltip->setStyleSheet( "padding: 5px; border: 1px solid white; border-radius: 3px; background-color: rgba(0, 0, 0, 0.9);" );
    mTooltip->setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );

    QTimer::singleShot(
        100,
        this,
        &MenuPluginWidget::connectToMenu
    );

    connect(
        this, &DesQ::Panel::PluginWidget::clicked, [ = ] () {
            if ( menuIpc ) {
                menuIpc->sendMessage( "Toggle" );
            }
        }
    );

    connect(
        this, &DesQ::Panel::PluginWidget::entered, [ = ] () {
            if ( mTooltip->isVisible() ) {
                return;
            }

            emit showTooltip( mTooltip );
        }
    );

    connect(
        this, &DesQ::Panel::PluginWidget::exited, [ = ] () {
            if ( mTooltip->isVisible() ) {
                mTooltip->close();
            }
        }
    );
}


MenuPluginWidget::~MenuPluginWidget() {
    if ( mTooltip ) {
        mTooltip->close();
    }

    delete menu;
    delete mTooltip;
}


void MenuPluginWidget::connectToMenu() {
    QString orgName = "DesQ";
    QString appName = "Menu";

    /** Create the socket path */
    QString sockPath( "%1/%2.socket" );
    QString sockDir( "%1/%2/%3" );

    sockDir = sockDir.arg( QStandardPaths::writableLocation( QStandardPaths::RuntimeLocation ) )
                 .arg( orgName.replace( " ", "-" ) )
                 .arg( QString( qgetenv( "XDG_SESSION_ID" ) ) );

    sockPath = sockPath.arg( sockDir ).arg( appName.replace( " ", "-" ) );
    menuIpc  = new DFL::IPC::Client( sockPath, this );

    menuIpc->connectToServer();

    /** Wait 100 ms for connection */
    if ( not menuIpc->waitForRegistered( 100 * 1000 ) ) {
        delete menu;
        menuIpc = nullptr;
    }
}
