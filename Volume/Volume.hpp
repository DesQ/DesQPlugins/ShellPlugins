/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"

#include "VolumeBackend.hpp"
#include "VolumeWidget.hpp"

#include <QtPlugin>
#include <desqui/PanelPlugin.hpp>

/* PulseAudio Context */
#include <pulsebridge/context.h>
#include <pulsebridge/server.h>
#include <pulsebridge/sink.h>
#include <pulsebridge/source.h>
#include <pulsebridge/port.h>

class VolumePluginWidget : public DesQ::Panel::PluginWidget {
    Q_OBJECT;

    public:
        VolumePluginWidget( QWidget *parent );
        ~VolumePluginWidget();

    private:
        /** Chirp when the volume is changed */
        void chirp();

        /** Notify the user: system notification */
        void notifyUser( int );

        /** Pulse Audio context */
        const QString mID = "141";

        /** Volume label */
        QLabel *volume;

        /** Volume popup and tooltip */
        VolumeWidget *mPopup;
        QLabel *mTooltip;

        /** Volume backend */
        VolumeBackend *backend;
};


class VolumePlugin : public QObject, public DesQ::Plugin::PanelInterface {
    Q_OBJECT;

    Q_PLUGIN_METADATA( IID "org.DesQ.Plugin.Panel" );
    Q_INTERFACES( DesQ::Plugin::PanelInterface );

    public:
        /* Name of the plugin */
        QString name() override {
            return "Volume";
        }

        /* Icon for the plugin */
        QIcon icon() override {
            return QIcon::fromTheme( "audio-speaker-symbolic" );
        }

        /* The plugin version */
        QString version() override {
            return "0.0.8";
        }

        /* The clock Widget */
        DesQ::Panel::PluginWidget *widget( QWidget *parent ) override {
            return new VolumePluginWidget( parent );
        }
};
