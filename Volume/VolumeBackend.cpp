/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtCore>
#include <canberra.h>

#include "VolumeBackend.hpp"

VolumeBackend::VolumeBackend( QObject *parent ) : QObject( parent ) {
    /* Init defaultSink, lastUsed's to 0 */
    defaultSink       = nullptr;
    defaultSource     = nullptr;
    currentSinkPort   = nullptr;
    currentSourcePort = nullptr;
}


VolumeBackend::~VolumeBackend() {
    for ( PulseAudioQt::Port *port: sinkPorts ) {
        port->disconnect();
        delete port;
    }

    for ( PulseAudioQt::Port *port: sourcePorts ) {
        port->disconnect();
        delete port;
    }

    delete defaultSink;
    delete defaultSource;
}


void VolumeBackend::setSinkVolume( qreal vol ) {
    int aVol = ( int )(vol * PulseAudioQt::normalVolume() / 100);

    if ( defaultSink ) {
        defaultSink->setVolume( aVol );
    }
}


void VolumeBackend::setSourceVolume( qreal vol ) {
    int aVol = ( int )(vol * PulseAudioQt::normalVolume() / 100);

    if ( defaultSource ) {
        defaultSource->setVolume( aVol );
    }
}


void VolumeBackend::toggleSinkMuted() {
    if ( defaultSink ) {
        defaultSink->setMuted( not defaultSink->isMuted() );
    }
}


void VolumeBackend::toggleSourceMuted() {
    if ( defaultSource ) {
        defaultSource->setMuted( not defaultSource->isMuted() );
    }
}


void VolumeBackend::initialize() {
    PulseAudioQt::Context *paCtx = PulseAudioQt::Context::instance();
    PulseAudioQt::Server  *paSrv = paCtx->server();

    connect(
        paSrv, &PulseAudioQt::Server::defaultSinkChanged, [ = ]( PulseAudioQt::Sink *sink ) {
            if ( defaultSink ) {
                /* Disconnect the older sink */
                defaultSink->disconnect();
            }

            /* Reset the sink */
            defaultSink = sink;

            /** defaultSink can be nullptr; beware */
            if ( defaultSink == nullptr ) {
                return;
            }

            emitVolumeIcon();

            /* Default port: handleSinkPorts() performs an init :P */
            handleSinkPorts();

            /* Initialzie the mute state */
            manageSinkMute();

            /** We will not allow the sink volume to go higher than 150 */
            connect(
                defaultSink, &PulseAudioQt::Sink::volumeChanged, [ = ]() {
                    qreal newVol = 100.0 * sink->volume() / PulseAudioQt::normalVolume();

                    if ( newVol > 150 ) {
                        setSinkVolume( 150 );
                        newVol = 150;
                    }

                    emitVolumeIcon();
                }
            );

            connect( defaultSink, &PulseAudioQt::Sink::mutedChanged, this, &VolumeBackend::manageSinkMute );
            connect( defaultSink, &PulseAudioQt::Sink::portsChanged, this, &VolumeBackend::handleSinkPorts );
        }
    );

    connect(
        paSrv, &PulseAudioQt::Server::defaultSourceChanged, [ = ]( PulseAudioQt::Source *source ) {
            if ( defaultSource ) {
                /* Disconnect the older source */
                defaultSource->disconnect();
            }

            /* Reset the source */
            defaultSource = source;

            /** defaultSource can be nullptr; beware */
            if ( defaultSource == nullptr ) {
                return;
            }

            /* Reset the volume */
            emit sourceVolumeChanged( 100.0 * source->volume() / PulseAudioQt::normalVolume() );

            /* Default port: handlesourcePorts() performs an init :P */
            handleSourcePorts();

            /* Initialzie the mute state */
            manageSourceMute();

            /** We will not allow the source volume to go higher than 150 */
            connect(
                defaultSource, &PulseAudioQt::Source::volumeChanged, [ = ]() {
                    setSourceVolume( 150 );
                    emit sourceVolumeChanged( 150 );
                }
            );

            connect( defaultSource, &PulseAudioQt::Source::mutedChanged, this, &VolumeBackend::manageSourceMute );
            connect( defaultSource, &PulseAudioQt::Source::portsChanged, this, &VolumeBackend::handleSourcePorts );
        }
    );
}


void VolumeBackend::manageSinkMute() {
    if ( defaultSink == nullptr ) {
        return;
    }

    emit sinkMuteChanged( defaultSink->isMuted() );

    emitVolumeIcon( true );
}


void VolumeBackend::handleSinkPorts() {
    /////
    // TODO
    // We should include code to mute/pause audio when the headphones/headset are removed
    // We should also include code to mute/pause/reduce-volume if the volume appears too high
    /////

    /////
    // Ports choice algo
    // 1. Check if headset/headphones are available. If yes, set that as the active port.
    // 2. Otherwise, get the port with highest priority which is not Unavailable. Set that as active port.
    // 3. If we just removed the headphones/headset, pause all streams/or mute audio. (User setting)
    /////

    if ( not defaultSink ) {
        return;
    }

    /* Disconnect all port signals */
    sinkPorts.clear();

    /* Re-init the list */
    sinkPorts = defaultSink->ports().toList();
    for ( PulseAudioQt::Port *port: sinkPorts ) {
        connect( port, &PulseAudioQt::Port::availabilityChanged, this, &VolumeBackend::handleSinkPorts );
    }

    /* Start searching for the default port to use */
    PulseAudioQt::Port *port    = nullptr;
    quint32            priority = 0;

    /* Headphone/headset Search: Choose the highest priority headphones */
    for ( PulseAudioQt::Port *p: sinkPorts ) {
        if ( p->name().toLower().contains( "headphone" ) or p->description().toLower().contains( "headphone" ) ) {
            if ( p->availability() == PulseAudioQt::Port::Unavailable ) {
                continue;
            }

            if ( p->priority() > priority ) {
                priority = p->priority();
                port     = p;
                break;
            }
        }

        else if ( p->name().toLower().contains( "headset" ) or p->description().toLower().contains( "headset" ) ) {
            if ( p->availability() == PulseAudioQt::Port::Unavailable ) {
                continue;
            }

            if ( p->priority() > priority ) {
                priority = p->priority();
                port     = p;
                break;
            }
        }
    }

    /* If it's a headphone/headset, and is different from the the previous port */
    if ( port and (port != currentSinkPort) ) {
        currentSinkPort = port;

        /* Set the active port */
        defaultSink->setActivePortIndex( defaultSink->ports().indexOf( port ) );

        /* Set suitable icon */
        emit sinkIconChanged( "audio-headphones" );

        /* Remember the last used port */
        if ( port->description().toLower().contains( "headphone" ) ) {
            lastUsedSinkPort = "headphone";
        }

        else if ( port->description().toLower().contains( "headset" ) ) {
            lastUsedSinkPort = "headphone";
        }

        else {
            lastUsedSinkPort = "speakers";
        }

        /* Get the volume of the port */
        qreal volPC = 100.0 * defaultSink->volume() / PulseAudioQt::normalVolume();

        /* Set the high volume alert */
        if ( volPC >= 75 ) {
            qWarning() << "High volume alert!!";
        }

        /* Set the volume on the @sinkVolBar */
        emitVolumeIcon();

        return;
    }

    /* Currently headphones are not connected. Check if @lastUsedSinkPort was headphones or headset */
    if ( lastUsedSinkPort == "headphone" ) {
        // if ( theUserWantsIt )
        QProcess::startDetached( "playerctl", { "pause" } );

        // if ( aggressive )
        // defaultSink->setMuted( true )
    }

    port     = nullptr;
    priority = 0;

    /* Choose the highest device which is not unavailable */
    for ( PulseAudioQt::Port *p: defaultSink->ports() ) {
        if ( p->availability() == PulseAudioQt::Port::Unavailable ) {
            continue;
        }

        if ( p->priority() > priority ) {
            priority = p->priority();
            port     = p;
            break;
        }
    }

    /* If we have a valid port, and one different from the previous */
    if ( port and (port != currentSinkPort) ) {
        currentSinkPort = port;

        /* Set the active port */
        defaultSink->setActivePortIndex( defaultSink->ports().indexOf( port ) );

        /* Set suitable icon */
        emit sinkIconChanged( "audio-speakers-symbolic" );

        /* Remember the last used port */
        if ( port->description().toLower().contains( "headphone" ) ) {
            lastUsedSinkPort = "headphone";
        }

        else if ( port->description().toLower().contains( "headset" ) ) {
            lastUsedSinkPort = "headphone";
        }

        else {
            lastUsedSinkPort = "speakers";
        }

        /* Get the volume of the port */
        qreal volPC = 100.0 * defaultSink->volume() / PulseAudioQt::normalVolume();

        /* Set the high volume alert */
        if ( volPC >= 75 ) {
            qWarning() << "High volume alert!!";
        }

        /* Set the volume on the @sinkVolBar */
        emitVolumeIcon();

        return;
    }
}


void VolumeBackend::manageSourceMute() {
    if ( defaultSource == nullptr ) {
        return;
    }

    emit sourceMuteChanged( defaultSource->isMuted() );
}


void VolumeBackend::handleSourcePorts() {
    /////
    // TODO
    // We should include code to mute/pause audio when the headphones/headset are removed
    // We should also include code to mute/pause/reduce-volume if the volume appears too high
    /////

    /////
    // Ports choice algo
    // 1. Check if headset/headphones are available. If yes, set that as the active port.
    // 2. Otherwise, get the port with highest priority which is not Unavailable. Set that as active port.
    // 3. If we just removed the headphones/headset, pause all streams/or mute audio. (User setting)
    /////

    if ( not defaultSource ) {
        return;
    }

    /* Disconnect all port signals */
    sourcePorts.clear();

    /* Re-init the list */
    sourcePorts = defaultSource->ports().toList();
    for ( PulseAudioQt::Port *port: sourcePorts ) {
        connect( port, &PulseAudioQt::Port::availabilityChanged, this, &VolumeBackend::handleSourcePorts );
    }

    /* Start searching for the default port to use */
    PulseAudioQt::Port *port    = nullptr;
    quint32            priority = 0;

    /* Headphone/headset Search: Choose the highest priority headphones */
    for ( PulseAudioQt::Port *p: sourcePorts ) {
        if ( p->name().toLower().contains( "headphone" ) or p->description().toLower().contains( "headphone" ) ) {
            if ( p->availability() == PulseAudioQt::Port::Unavailable ) {
                continue;
            }

            if ( p->priority() > priority ) {
                priority = p->priority();
                port     = p;
                break;
            }
        }

        else if ( p->name().toLower().contains( "headset" ) or p->description().toLower().contains( "headset" ) ) {
            if ( p->availability() == PulseAudioQt::Port::Unavailable ) {
                continue;
            }

            if ( p->priority() > priority ) {
                priority = p->priority();
                port     = p;
                break;
            }
        }
    }

    /* If it's a headphone/headset */
    if ( port and (port != currentSourcePort) ) {
        currentSourcePort = port;

        /* Set the active port */
        defaultSource->setActivePortIndex( defaultSource->ports().indexOf( port ) );

        /* Set suitable icon */
        emit sourceIconChanged( "audio-headphones" );

        /* Remember the last used port */
        if ( port->description().toLower().contains( "headphone" ) ) {
            lastUsedSourcePort = "headphone";
        }

        else if ( port->description().toLower().contains( "headset" ) ) {
            lastUsedSourcePort = "headphone";
        }

        else {
            lastUsedSourcePort = "speakers";
        }

        /* Set the volume on the @sinkVolBar */
        emit sourceVolumeChanged( 100.0 * defaultSource->volume() / PulseAudioQt::normalVolume() );

        return;
    }
}


void VolumeBackend::emitVolumeIcon( bool muteEvent ) {
    if ( defaultSink == nullptr ) {
        return;
    }

    if ( defaultSink->isMuted() ) {
        emit volumeIconChanged( "audio-volume-muted" );
        return;
    }

    qreal newVol = 100.0 * defaultSink->volume() / PulseAudioQt::normalVolume();

    QString icon;

    if ( newVol == 0 ) {
        icon = "audio-volume-muted";
    }

    else if ( newVol <= 33 ) {
        icon = "audio-volume-low";
    }

    else if ( newVol <= 66 ) {
        icon = "audio-volume-medium";
    }

    else {
        icon = "audio-volume-high";
    }

    emit volumeIconChanged( icon );

    if ( not muteEvent ) {
        emit sinkVolumeChanged( newVol );
    }
}
