/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QList>
#include <QObject>
#include <QString>

/* PulseAudio Context */
#include <DFL/DF5/pulsebridge/context.h>
#include <DFL/DF5/pulsebridge/server.h>
#include <DFL/DF5/pulsebridge/sink.h>
#include <DFL/DF5/pulsebridge/source.h>
#include <DFL/DF5/pulsebridge/port.h>

class VolumeBackend : public QObject {
    Q_OBJECT;

    public:
        VolumeBackend( QObject *parent );
        ~VolumeBackend();

        void setSinkVolume( qreal );
        void setSourceVolume( qreal );

        void toggleSinkMuted();
        void toggleSourceMuted();

        void initialize();

    private:
        /** Audio variables */
        PulseAudioQt::Sink *defaultSink;
        PulseAudioQt::Port *currentSinkPort;
        QList<PulseAudioQt::Port *> sinkPorts;
        QString lastUsedSinkPort;
        bool isSinkMuted = false;

        /** Audio variables */
        PulseAudioQt::Source *defaultSource;
        PulseAudioQt::Port *currentSourcePort;
        QList<PulseAudioQt::Port *> sourcePorts;
        QString lastUsedSourcePort;
        bool isSourceMuted = false;

        void manageSinkMute();
        void handleSinkPorts();

        void manageSourceMute();
        void handleSourcePorts();

        void emitVolumeIcon( bool muteEvent = false );

    Q_SIGNALS:

        /**
         * The Following three signals are for the GUI elements to change respective icons.
         */

        /**
         * Volume icon for current volume
         */
        void volumeIconChanged( QString );

        /**
         * Sink icon: Currently used sink port
         */
        void sinkIconChanged( QString );

        /**
         * Source icon: Currently used mic port
         */
        void sourceIconChanged( QString );

        /**
         * Volume of the Sink (i.e. Speaker/Headphone) changed.
         * Update the GUI volume bar
         */
        void sinkVolumeChanged( qreal );

        /**
         * Volume of the Source (i.e. microphone) changed.
         * Update the GUI volume bar
         */
        void sourceVolumeChanged( qreal );

        /**
         * The Sink was muted.
         * Note: This is not the same as sinkVolumeChanged( 0 ).
         * Mute implies that no signals will be set to the sink.
         * Volume can be finite or zero.
         */
        void sinkMuteChanged( bool );

        /**
         * The Source was muted.
         * Note: This is not the same as sourceVolumeChanged( 0 ).
         * Mute implies that no signals will be processed from the source.
         * Volume can be finite or zero.
         */
        void sourceMuteChanged( bool );
};
