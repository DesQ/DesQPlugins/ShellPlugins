/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QWidget>
#include <QLabel>
#include <QSlider>
#include <QToolButton>

class VolumeWidget : public QWidget {
    Q_OBJECT

    public:
        VolumeWidget();
        ~VolumeWidget();

        void setSinkVolume( int vol );
        void setSourceVolume( int vol );

        void setSinkMuted( bool muted );
        void setSourceMuted( bool muted );

        void setSinkIcon( QString icon );
        void setSourceIcon( QString icon );

    private:
        QToolButton *sinkDevIcon;
        QLabel *sinkDevName;
        QSlider *sinkVolBar;

        QToolButton *sourceDevIcon;
        QLabel *sourceDevName;
        QSlider *sourceVolBar;

        int mSinkVolume   = 50;
        int mSourceVolume = 50;

    Q_SIGNALS:
        void changeSinkVolume( int newVol );
        void toggleSinkMuted();

        void changeSourceVolume( int newVol );
        void toggleSourceMuted();

    protected:
        void keyReleaseEvent( QKeyEvent *kEvent );
};
