/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>

#include "VolumeWidget.hpp"
#include "Common/Widgets.hpp"

VolumeWidget::VolumeWidget() : QWidget() {
    sinkDevName = new QLabel( "Sink" );

    sinkDevIcon = new QToolButton();
    sinkDevIcon->setIcon( QIcon::fromTheme( "audio-ready" ) );
    sinkDevIcon->setIconSize( QSize( 16, 16 ) );

    sinkVolBar = new QSlider( Qt::Horizontal );
    sinkVolBar->setTracking( false );
    sinkVolBar->setRange( 0, 150 );
    sinkVolBar->setTickInterval( 100 );
    sinkVolBar->setTickPosition( QSlider::TicksBelow );

    sourceDevName = new QLabel( "Source" );

    sourceDevIcon = new QToolButton();
    sourceDevIcon->setIcon( QIcon::fromTheme( "mic-ready" ) );
    sourceDevIcon->setIconSize( QSize( 16, 16 ) );

    sourceVolBar = new QSlider( Qt::Horizontal );
    sourceVolBar->setTracking( false );
    sourceVolBar->setRange( 0, 100 );

    QGridLayout *baseLyt = new QGridLayout();

    baseLyt->addWidget( sinkDevName,             0, 0, 1, 2 );
    baseLyt->addWidget( sinkDevIcon,             1, 0 );
    baseLyt->addWidget( sinkVolBar,              1, 1 );

    baseLyt->addWidget( Separator::horizontal(), 2, 1 );

    baseLyt->addWidget( sourceDevName,           3, 0, 1, 2 );
    baseLyt->addWidget( sourceDevIcon,           4, 0 );
    baseLyt->addWidget( sourceVolBar,            4, 1 );

    setLayout( baseLyt );

    connect( sinkDevIcon,   &QToolButton::clicked, this, &VolumeWidget::toggleSinkMuted );
    connect( sourceDevIcon, &QToolButton::clicked, this, &VolumeWidget::toggleSourceMuted );

    connect( sinkVolBar,    &QSlider::sliderMoved, this, &VolumeWidget::changeSinkVolume );
    connect( sourceVolBar,  &QSlider::sliderMoved, this, &VolumeWidget::changeSourceVolume );

    /** To make this surface suitable to be shown as a layer shell */
    setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );
}


VolumeWidget::~VolumeWidget() {
    delete sinkDevIcon;
    delete sinkDevName;
    delete sinkVolBar;

    delete sourceDevIcon;
    delete sourceDevName;
    delete sourceVolBar;
}


void VolumeWidget::setSinkVolume( int vol ) {
    if ( sinkVolBar->isSliderDown() ) {
        return;
    }

    sinkVolBar->setValue( vol );
    mSinkVolume = vol;
}


void VolumeWidget::setSourceVolume( int vol ) {
    if ( sourceVolBar->isSliderDown() ) {
        return;
    }

    sourceVolBar->setValue( vol );
    mSourceVolume = vol;
}


void VolumeWidget::setSinkMuted( bool muted ) {
    if ( muted ) {
        sinkVolBar->setDisabled( true );
        setSinkIcon( "audio-off" );
    }

    else {
        sinkVolBar->setEnabled( true );

        QString icon;

        if ( mSinkVolume == 0 ) {
            icon = "audio-volume-muted";
        }

        else if ( mSinkVolume <= 33 ) {
            icon = "audio-volume-low";
        }

        else if ( mSinkVolume <= 66 ) {
            icon = "audio-volume-medium";
        }

        else {
            icon = "audio-volume-high";
        }

        setSinkIcon( icon );
    }
}


void VolumeWidget::setSourceMuted( bool muted ) {
    if ( muted ) {
        sourceVolBar->setDisabled( true );
        setSourceIcon( "mic-off" );
    }

    else {
        sourceVolBar->setEnabled( true );

        QString icon;

        if ( mSinkVolume == 0 ) {
            icon = "microphone-sensitivity-muted";
        }

        else if ( mSinkVolume <= 33 ) {
            icon = "microphone-sensitivity-low";
        }

        else if ( mSinkVolume <= 66 ) {
            icon = "microphone-sensitivity-medium";
        }

        else {
            icon = "microphone-sensitivity-high";
        }

        setSourceIcon( icon );
    }
}


void VolumeWidget::setSinkIcon( QString icon ) {
    sinkDevIcon->setIcon( QIcon::fromTheme( icon ) );
}


void VolumeWidget::setSourceIcon( QString icon ) {
    sourceDevIcon->setIcon( QIcon::fromTheme( icon ) );
}


void VolumeWidget::keyReleaseEvent( QKeyEvent *kEvent ) {
    if ( kEvent->key() == Qt::Key_Escape ) {
        close();
    }

    QWidget::keyReleaseEvent( kEvent );
}
