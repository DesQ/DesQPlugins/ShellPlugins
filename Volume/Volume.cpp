/**
 * This file is a part of DesQ Volume.
 * DesQ Volume is the Volume Manager for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Volume.hpp"
#include <canberra.h>

VolumePluginWidget::VolumePluginWidget( QWidget *parent ) : DesQ::Panel::PluginWidget( parent ) {
    backend = new VolumeBackend( this );

    volume = new QLabel();
    volume->setFixedSize( panelHeight, panelHeight );
    volume->setAlignment( Qt::AlignCenter );

    volume->setPixmap( QIcon::fromTheme( "audio-ready" ).pixmap( panelHeight - 4 ) );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins() );
    lyt->addWidget( volume );
    setLayout( lyt );

    mPopup   = new VolumeWidget();
    mTooltip = new QLabel();

    mTooltip->setStyleSheet( "padding: 5px; border: 1px solid white; border-radius: 3px; background-color: rgba(0, 0, 0, 0.9);" );
    mTooltip->setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );

    connect(
        backend, &VolumeBackend::volumeIconChanged, [ = ] ( QString iconStr ) {
            volume->setPixmap( QIcon::fromTheme( iconStr ).pixmap( panelHeight - 4 ) );
            mPopup->setSinkIcon( iconStr );
        }
    );

    connect(
        backend, &VolumeBackend::sinkVolumeChanged, [ = ] ( qreal newVol ) {
            chirp();
            mPopup->setSinkVolume( (int)newVol );
            notifyUser( (int)newVol );

            mTooltip->setText( QString( "Volume: %1%" ).arg( (int)newVol ) );
        }
    );

    connect(
        backend, &VolumeBackend::sourceVolumeChanged, [ = ] ( qreal newVol ) {
            mPopup->setSourceVolume( (int)newVol );
        }
    );

    connect(
        backend, &VolumeBackend::sinkIconChanged, [ = ] ( QString icon ) {
            mPopup->setSinkIcon( icon );
        }
    );

    connect(
        backend, &VolumeBackend::sourceIconChanged, [ = ] ( QString icon ) {
            mPopup->setSourceIcon( icon );
        }
    );

    connect(
        backend, &VolumeBackend::sinkMuteChanged, [ = ] ( bool muted ) {
            mPopup->setSinkMuted( muted );

            if ( muted ) {
                mTooltip->setText( QString( "Volume: muted" ) );
            }

            if ( not muted ) {
                chirp();
            }
        }
    );

    connect(
        backend, &VolumeBackend::sourceMuteChanged, [ = ] ( bool muted ) {
            mPopup->setSourceMuted( muted );
        }
    );

    connect( mPopup, &VolumeWidget::changeSinkVolume,   backend, &VolumeBackend::setSinkVolume );
    connect( mPopup, &VolumeWidget::toggleSinkMuted,    backend, &VolumeBackend::toggleSinkMuted );
    connect( mPopup, &VolumeWidget::changeSourceVolume, backend, &VolumeBackend::setSourceVolume );
    connect( mPopup, &VolumeWidget::toggleSourceMuted,  backend, &VolumeBackend::toggleSourceMuted );

    backend->initialize();

    connect(
        this, &DesQ::Panel::PluginWidget::clicked, [ = ] () {
            if ( mPopup->isVisible() ) {
                mPopup->close();
            }

            else {
                emit showPopup( mPopup );
            }
        }
    );

    connect(
        this, &DesQ::Panel::PluginWidget::entered, [ = ] () {
            if ( mTooltip->isVisible() ) {
                return;
            }

            emit showTooltip( mTooltip );
        }
    );

    connect(
        this, &DesQ::Panel::PluginWidget::exited, [ = ] () {
            if ( mTooltip->isVisible() ) {
                mTooltip->close();
            }
        }
    );
}


VolumePluginWidget::~VolumePluginWidget() {
    /** Disconnect all signals and slots */
    disconnect();

    /** Delete the volume label */
    delete volume;
}


void VolumePluginWidget::chirp() {
    qDebug() << "Chirping";

    /**
     * A small short sound to indicate volume audibly
     * For now, just print it on the screen
     */
    ca_context *chirp;

    ca_context_create( &chirp );
    ca_context_set_driver( chirp, "pulse" );
    ca_context_play(
        chirp,
        0,
        CA_PROP_EVENT_ID,
        "message",
        CA_PROP_EVENT_DESCRIPTION,
        "Chirp",
        NULL
    );
}


void VolumePluginWidget::notifyUser( int newVol ) {
    QString icon;

    if ( newVol == 0 ) {
        icon = "audio-volume-muted";
    }

    else if ( newVol <= 33 ) {
        icon = "audio-volume-low";
    }

    else if ( newVol <= 66 ) {
        icon = "audio-volume-medium";
    }

    else {
        icon = "audio-volume-high";
    }

    if ( newVol > 100 ) {
        newVol = 100.0;
    }

    QString hint( "--hint=double:percent:%1" );

    QProcess::startDetached(
        "desq-notifier", {
            "-i", icon, "-a", "desq Volume",
            "-t", "5000", "-u", "normal", "-r", mID,
            hint.arg( newVol ), "Volume", ""
        }
    );
}
