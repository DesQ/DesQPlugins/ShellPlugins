/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>
#include <desq/SysInfo.hpp>
#include <desqui/ShellPlugin.hpp>

#include <sensors/sensors.h>
#include <sensors/error.h>

#include "ResourcesWidget.hpp"

QSettings resWdgtSett( QDir::home().filePath( ".config/DesQ/ContainerPluginResources.conf" ), QSettings::IniFormat );

ResourcesWidget::ResourcesWidget( QWidget *parent ) : DesQ::Shell::PluginWidget( parent ) {
    mSysInfo = DesQ::SystemInfo::sysInfoObject();
    mTimer   = new QBasicTimer();

    numRes = 3;
    labels = QStringList( { "CPU", "RAM", "Temp" } );
    units  = QStringList( { "%", "%", "°C" } );

    QGridLayout *lyt = new QGridLayout();

    for ( int i = 0; i < numRes; i++ ) {
        QProgressBar *pb = new QProgressBar( this );
        pb->setSizePolicy( QSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::Fixed ) );
        pb->setOrientation( Qt::Horizontal );
        pb->setAlignment( Qt::AlignCenter );
        pb->setMinimumWidth( 150 );
        pb->setFixedHeight( 20 );
        pb->setStyleSheet(
            "QProgressBar{"
            "    border: none;"
            "    background-color: rgba(50, 50, 50, 72);"
            "    border-radius: 10px;"
            "    width: 20px;"
            "}"
            "QProgressBar::chunk {"
            "    background-color: palette(Highlight);"
            "    border-radius: 10px;"
            "}"
        );
        pb->setRange( 0, 100 );
        pb->setFormat( labels.at( i ) + ": %p " + units.at( i ) );
        resWidgets << pb;

        lyt->addWidget( pb, i, 0, Qt::AlignCenter );
    }

    setLayout( lyt );

    opacity = new QGraphicsOpacityEffect( this );
    opacity->setOpacity( 0.4 );
    setGraphicsEffect( opacity );

    mTimer->start( 1000, this );

    setMouseTracking( true );

    setFixedSize( QSize( 200, 75 ) );
}


void ResourcesWidget::timerEvent( QTimerEvent *tEvent ) {
    if ( tEvent->timerId() == mTimer->timerId() ) {
        QList<qint64> percents;
        percents << mSysInfo->getCpuUsage().at( 0 );

        QList<qint64> ram = mSysInfo->getRamUsage();

        if ( ram[ 1 ] ) {
            percents << 100.0 * ram[ 0 ] / ram[ 1 ];
        }

        else {
            percents << 0;
        }

        percents << readThermalInfo();

        for ( int i = 0; i < numRes; i++ ) {
            resWidgets[ i ]->setValue( percents.at( i ) );
        }
    }

    QWidget::timerEvent( tEvent );
}


void ResourcesWidget::enterEvent( QEvent *event ) {
    opacity->setOpacity( 1.0 );
    event->accept();
}


void ResourcesWidget::leaveEvent( QEvent *event ) {
    opacity->setOpacity( 0.4 );
    event->accept();
}


qint64 ResourcesWidget::readThermalInfo() {
    sensors_cleanup();

    int err = sensors_init( nullptr );

    if ( err != 0 ) {
        qCritical() << "Error:" << sensors_strerror( err );
        return 0;
    }

    int  chipNumber = 0;
    char buf[ 256 ] { 0 };
    const sensors_chip_name  *chip;
    const sensors_subfeature *sub;

    while ( (chip = sensors_get_detected_chips( nullptr, &chipNumber ) ) ) {
        if ( sensors_snprintf_chip_name( buf, 256, chip ) < 0 ) {
            snprintf( buf, 256, "%i", chipNumber );
        }

        if ( buf != resWdgtSett.value( "Chip" ).toString() ) {
            continue;
        }

        const char *adap = sensors_get_adapter_name( &chip->bus );

        if ( adap != resWdgtSett.value( "Adaptor" ).toString() ) {
            continue;
        }

        int num = 0;
        const sensors_feature *feature;

        while ( (feature = sensors_get_features( chip, &num ) ) ) {
            char   *str = sensors_get_label( chip, feature );
            double temp = 0.0;

            if ( str != resWdgtSett.value( "Feature" ).toString() ) {
                continue;
            }

            sub = sensors_get_subfeature( chip, feature, (sensors_subfeature_type)( ( (int)feature->type) << 8) );

            sensors_get_value( chip, sub->number, &temp );
            free( str );

            return temp;
        }
    }

    return 50.0;
}
