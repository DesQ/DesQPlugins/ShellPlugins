/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include <desqui/PanelPlugin.hpp>

#include <QtPlugin>

class ClockPluginWidget : public DesQ::Panel::PluginWidget {
    Q_OBJECT;

    public:
        ClockPluginWidget( QWidget *parent = NULL );
        ~ClockPluginWidget();

    private:
        QLabel *clock;
        QBasicTimer *cTimer;
        QFontMetrics *fm;

        QCalendarWidget *mPopup;
        QLabel *mTooltip;

    protected:
        void timerEvent( QTimerEvent * ) override;
};


class ClockPlugin : public QObject, public DesQ::Plugin::PanelInterface {
    Q_OBJECT

    Q_PLUGIN_METADATA( IID "org.DesQ.Plugin.Panel" );
    Q_INTERFACES( DesQ::Plugin::PanelInterface );

    public:
        /* Name of the plugin */
        QString name() override {
            return "Clock";
        }

        /* Icon for the plugin */
        QIcon icon() override {
            return QIcon::fromTheme( "clock" );
        }

        /* The plugin version */
        QString version() override {
            return "0.0.8";
        }

        /* The clock Widget */
        DesQ::Panel::PluginWidget *widget( QWidget *parent ) override {
            return new ClockPluginWidget( parent );
        }
};
