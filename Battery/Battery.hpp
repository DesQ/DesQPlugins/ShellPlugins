/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"
#include <desqui/PanelPlugin.hpp>

namespace DFL {
    namespace Power {
        class Manager;
    }
}

#include <QtPlugin>

class BatteryPluginWidget : public DesQ::Panel::PluginWidget {
    Q_OBJECT;

    public:
        BatteryPluginWidget( QWidget *parent = NULL );
        ~BatteryPluginWidget();

    private:
        QLabel *battery;

        DFL::Power::Manager *power;

        QLabel *mTooltip;
        const QString mID = "78";

        void connectToUPower();
        void notifyUser( QString title, QString message, short weight = 1 );
};


class BatteryPlugin : public QObject, public DesQ::Plugin::PanelInterface {
    Q_OBJECT

    Q_PLUGIN_METADATA( IID "org.DesQ.Plugin.Panel" );
    Q_INTERFACES( DesQ::Plugin::PanelInterface );

    public:
        /* Name of the plugin */
        QString name() override {
            return "Battery";
        }

        /* Icon for the plugin */
        QIcon icon() override {
            return QIcon::fromTheme( "desq" );
        }

        /* The plugin version */
        QString version() override {
            return "0.0.8";
        }

        /* The Battery Widget */
        DesQ::Panel::PluginWidget *widget( QWidget *parent ) override {
            return new BatteryPluginWidget( parent );
        }
};
