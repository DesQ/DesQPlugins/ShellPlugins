/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>
#include <DFL/DF5/PowerManager.hpp>

#include "Battery.hpp"

static inline QPixmap updateIcon( double value, bool onAC ) {
    /**
     * value -> Battery Percentage
     * onAC  -> AC Power Connected
     */

    if ( onAC and value > 99.0 ) {
        return QIcon::fromTheme( "desq-power-manager" ).pixmap( 128 );
    }

    QColor border;

    double mFraction = value / 100.0;
    int    red = 0, green = 0;

    // Critical: Bright Red
    if ( value < 5.0 ) {
        // Bright Red
        red   = 0xff;
        green = 0x00;
    }

    // Change from Red to Yellow: When mFraction = 0.05, green = 0; mFraction = 0.4, green = 0xBB
    else if ( mFraction < 0.4 ) {
        red   = 0xBB;
        green = ( int )( (mFraction - 0.05) * 550);
    }

    // Remain Yellow
    else if ( mFraction <= 0.6 ) {
        red   = 0xBB;
        green = 0xBB;
    }

    // Change from Yellow to Green: When totalF = 0.6, red = 0xBB; mFraction = 1, red = 0;
    else {
        red   = ( int )( (1 - mFraction) * 637.5);
        green = 0xBB;
    }

    border = QColor( red, green, 0 );

    /* Battery charge gradient */
    QLinearGradient gradient( QPoint( 100, 0 ), QPoint( 220, 0 ) );

    gradient.setColorAt( 0.0, border.darker( 120 ) );
    gradient.setColorAt( 0.4, border );
    gradient.setColorAt( 0.6, border );
    gradient.setColorAt( 1.0, border.darker( 120 ) );
    QBrush fill = QBrush( gradient );

    /* Clean the image */
    QPixmap  img( 320, 320 );
    QPainter p( &img );

    p.setRenderHints( QPainter::Antialiasing );
    img.fill( Qt::transparent );

    /* Border */
    p.save();
    p.setPen( QPen( border, 10, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
    p.drawRoundedRect( QRect( 90, 30, 140, 280 ), 10, 10 );
    p.setPen( Qt::NoPen );
    p.setBrush( border );
    p.drawRoundedRect( QRect( 120, 5, 80, 30 ), 10, 10 );
    p.restore();

    /* Charge of the battery */
    p.save();
    p.setPen( Qt::NoPen );
    p.setBrush( fill );
    p.drawRoundedRect( QRect( 100, 40 + 260 * (100 - value) / 100.0, 120, 260 * value / 100.0 ), 5, 5 );
    p.restore();

    if ( onAC ) {
        QPainterPath pp;
        pp.moveTo( QPoint( 200, 60 ) );
        pp.lineTo( QPoint( 120, 170 ) );
        pp.lineTo( QPoint( 200, 170 ) );
        pp.lineTo( QPoint( 120, 280 ) );

        p.save();
        p.setPen( QPen( QColor( "#CCCC00" ), 20, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
        p.setBrush( QColor( "#AABBBB00" ) );
        p.drawPath( pp );
        p.restore();
    }

    else if ( value < 5 and not onAC ) {
        /** Draw a red exclamation point */
        p.save();
        p.setPen( QPen( QColor( Qt::darkRed ), 30, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );
        p.drawLine( QPointF( 160, 60 ),  QPointF( 160, 200 ) );
        p.drawLine( QPointF( 160, 240 ), QPointF( 160, 250 ) );
        p.restore();
    }

    p.end();

    return img;
}


BatteryPluginWidget::BatteryPluginWidget( QWidget *parent ) : DesQ::Panel::PluginWidget( parent ) {
    battery = new QLabel();

    battery->setAlignment( Qt::AlignCenter );
    battery->setPixmap( QIcon::fromTheme( "desq-power-manager" ).pixmap( panelHeight - 4 ) );
    battery->setAlignment( Qt::AlignCenter );
    battery->setSizePolicy( QSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred ) );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins() );
    lyt->addWidget( battery );
    setLayout( lyt );

    mTooltip = new QLabel( "No batteries detected" );

    mTooltip->setStyleSheet( "padding: 5px; border: 1px solid white; border-radius: 3px; background-color: rgba(0, 0, 0, 0.9);" );
    mTooltip->setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );

    connectToUPower();

    connect(
        this, &DesQ::Panel::PluginWidget::entered, [ = ] () {
            if ( mTooltip->isVisible() ) {
                return;
            }

            emit showTooltip( mTooltip );
        }
    );

    connect(
        this, &DesQ::Panel::PluginWidget::exited, [ = ] () {
            if ( mTooltip->isVisible() ) {
                mTooltip->close();
            }
        }
    );
}


BatteryPluginWidget::~BatteryPluginWidget() {
    if ( mTooltip ) {
        mTooltip->close();
    }

    delete battery;
    delete mTooltip;
}


void BatteryPluginWidget::connectToUPower() {
    power = new DFL::Power::Manager( this );

    connect(
        power, &DFL::Power::Manager::lidStateChanged, [ = ] ( bool open ) {
            qDebug() << "lidStateChanged" << open;
        }
    );

    connect(
        power, &DFL::Power::Manager::switchedToBattery, [ = ] () {
            qDebug() << "switchedToBattery";
            notifyUser( "Battery Discharging", "Your battery is discharging.", 1 );
            mTooltip->setText( "Battery Discharging.<br>Battery Percentage: " + QString::number( (int)power->batteryCharge() ) );
        }
    );

    connect(
        power, &DFL::Power::Manager::switchedToACPower, [ = ] () {
            qDebug() << "switchedToACPower";
            notifyUser( "Battery Charging", "Power supply connected. Your battery is now charging.", 0 );
            mTooltip->setText( "Battery Charging.<br>Battery Percentage: " + QString::number( (int)power->batteryCharge() ) );
        }
    );

    connect(
        power, &DFL::Power::Manager::batteryChargeChanged, [ = ] ( double pc ) {
            qDebug() << "batteryChargeChanged" << pc;

            if ( power->onBattery() ) {
                mTooltip->setText( "Battery Discharging.<br>Battery Percentage: " + QString::number( (int)pc ) );
                battery->setPixmap( updateIcon( pc, false ).scaledToWidth( panelHeight - 4, Qt::SmoothTransformation ) );
            }

            else {
                mTooltip->setText( "Battery Charging.<br>Battery Percentage: " + QString::number( (int)pc ) );
                battery->setPixmap( updateIcon( pc, true ).scaledToWidth( panelHeight - 4, Qt::SmoothTransformation ) );
            }
        }
    );

    connect(
        power, &DFL::Power::Manager::timeToFull, [ = ] ( qlonglong etf ) {
            qDebug() << "timeToFull" << etf;
        }
    );

    connect(
        power, &DFL::Power::Manager::timeToEmpty, [ = ] ( qlonglong etr ) {
            qDebug() << "timeToEmpty" << etr;
        }
    );

    connect(
        power, &DFL::Power::Manager::batteryNearlyEmpty, [ = ] () {
            qDebug() << "batteryNearlyEmpty";
            notifyUser( "Battery Low", "Your battery is nearly empty. You're strongly advised to connect to a power supply.", 2 );
            mTooltip->setText( "Battery Low.<br>Battery Percentage: <font color='red'>" + QString::number( (int)power->batteryCharge() ) );
        }
    );

    connect(
        power, &DFL::Power::Manager::batteryEmpty, [ = ] () {
            qDebug() << "batteryEmpty";
            notifyUser( "Battery Critical", "Your battery is critically low. Please connect to a power source immediately.", 2 );
            mTooltip->setText( "Battery Critical.<br>Battery Percentage: <font color='red'>" + QString::number( (int)power->batteryCharge() ) );
        }
    );
}


void BatteryPluginWidget::notifyUser( QString title, QString message, short weight ) {
    QString urgency;

    switch ( weight ) {
        case 0: {
            urgency = "low";
            break;
        }

        case 1: {
            urgency = "normal";
            break;
        }

        default: {
            urgency = "critical";
            break;
        }
    }

    QProcess::startDetached(
        "desq-notifier", {
            "-i", "desq", "-a", "desq-power-manager",
            "-t", "5000", "-u", urgency, "-r", mID,
            title, message
        }
    );
}
