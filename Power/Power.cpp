/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>
#include <desq/Utils.hpp>

#include "Power.hpp"

PowerPluginWidget::PowerPluginWidget( QWidget *parent ) : DesQ::Panel::PluginWidget( parent ) {
    power = new QLabel();

    power->setPixmap( QIcon::fromTheme( "system-shutdown" ).pixmap( panelHeight - 4 ) );
    power->setAlignment( Qt::AlignCenter );
    power->setSizePolicy( QSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred ) );

    mTooltip = new QLabel( "Logout of DesQ Session" );

    mTooltip->setStyleSheet( "padding: 5px; border: 1px solid white; border-radius: 3px; background-color: rgba(0, 0, 0, 0.9);" );
    mTooltip->setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );

    QHBoxLayout *lyt = new QHBoxLayout();

    lyt->setContentsMargins( QMargins() );
    lyt->addWidget( power );
    setLayout( lyt );

    setMouseTracking( true );

    connect(
        this, &DesQ::Panel::PluginWidget::clicked, [ = ] () {
            QProcess::startDetached( DesQ::Utils::getUtilityPath( "logout" ), {} );
        }
    );

    connect(
        this, &DesQ::Panel::PluginWidget::entered, [ = ] () {
            if ( mTooltip->isVisible() ) {
                return;
            }

            emit showTooltip( mTooltip );
        }
    );

    connect(
        this, &DesQ::Panel::PluginWidget::exited, [ = ] () {
            if ( mTooltip->isVisible() ) {
                mTooltip->close();
            }
        }
    );
}


PowerPluginWidget::~PowerPluginWidget() {
    if ( mTooltip ) {
        mTooltip->close();
    }

    delete power;
    delete mTooltip;
}
