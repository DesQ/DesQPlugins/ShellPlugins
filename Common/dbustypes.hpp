/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QtCore>
#include <QtDBus>

/** List of the unsigned integers: Used for main window queries */
typedef QList<uint> QUIntList;

/** WorkSpace
 * Used to identify the workspace of a view
 * Used to identify the current workspace of an output
 */
typedef struct WorkSpace_t {
    int row    = -1;
    int column = -1;
} WorkSpace;

/** WorkSpaceGrid
 * Used to identify grid size of the workspaces of an output
 */
typedef struct WorkSpace_Grid_t {
    int rows    = 0;
    int columns = 0;
} WorkSpaceGrid;

/** List of WorkSpaces: To identify the WorkSpaces in which a view lives */
typedef QList<WorkSpace> WorkSpaces;

Q_DECLARE_METATYPE( QUIntList );
Q_DECLARE_METATYPE( WorkSpace );
Q_DECLARE_METATYPE( WorkSpaces );
Q_DECLARE_METATYPE( WorkSpaceGrid );

QDBusArgument &operator<<( QDBusArgument& argument, const WorkSpace& ws );
const QDBusArgument &operator>>( const QDBusArgument& argument, WorkSpace& ws );

QDBusArgument &operator<<( QDBusArgument& argument, const WorkSpaceGrid& ws );
const QDBusArgument &operator>>( const QDBusArgument& argument, WorkSpaceGrid& ws );

bool operator==( const WorkSpace& lhs, const WorkSpace& rhs );
bool operator<( const WorkSpace& lhs, const WorkSpace& rhs );

bool operator==( const WorkSpaceGrid& lhs, const WorkSpaceGrid& rhs );
