/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ project (https://gitlab.com/desq/)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "dbustypes.hpp"

// Marshall the WorkSpace data into a D-Bus argument
QDBusArgument &operator<<( QDBusArgument& argument, const WorkSpace& ws ) {
    argument.beginStructure();
    argument << ws.row;
    argument << ws.column;
    argument.endStructure();
    return argument;
}


// Retrieve the WorkSpace data from the D-Bus argument
const QDBusArgument &operator>>( const QDBusArgument& argument, WorkSpace& ws ) {
    argument.beginStructure();
    argument >> ws.row;
    argument >> ws.column;
    argument.endStructure();
    return argument;
}


// Marshall the WorkSpace data into a D-Bus argument
QDBusArgument &operator<<( QDBusArgument& argument, const WorkSpaceGrid& ws ) {
    argument.beginStructure();
    argument << ws.rows;
    argument << ws.columns;
    argument.endStructure();
    return argument;
}


// Retrieve the WorkSpace data from the D-Bus argument
const QDBusArgument &operator>>( const QDBusArgument& argument, WorkSpaceGrid& ws ) {
    argument.beginStructure();
    argument >> ws.rows;
    argument >> ws.columns;
    argument.endStructure();
    return argument;
}


bool operator==( const WorkSpace& lhs, const WorkSpace& rhs ) {
    return ( (lhs.row == rhs.row) && (lhs.column == rhs.column) );
}


bool operator<( const WorkSpace& lhs, const WorkSpace& rhs ) {
    if ( lhs.row < rhs.row ) {
        return true;
    }

    else if ( lhs.row == rhs.row ) {
        return (lhs.column < rhs.column);
    }

    else {
        return false;
    }
}


bool operator==( const WorkSpaceGrid& lhs, const WorkSpaceGrid& rhs ) {
    return ( (lhs.rows == rhs.rows) && (lhs.columns == rhs.columns) );
}
