/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <filesystem>

#include "MiscTools.hpp"

static inline QString getPixmapIcon( QString name ) {
    QStringList paths{
        "/usr/local/share/pixmaps/",
        "/usr/share/pixmaps/",
    };

    QStringList sfxs{
        ".svg", ".png", ".xpm"
    };

    for ( QString path: paths ) {
        for ( QString sfx: sfxs ) {
            if ( QFile::exists( path + name + sfx ) ) {
                return path + name + sfx;
            }
        }
    }

    return QString();
}


QIcon getIconForAppId( QString mAppId ) {
    if ( mAppId.isEmpty() or (mAppId == "Unknown") ) {
        return QIcon();
    }

    /** Wine apps */
    if ( mAppId.endsWith( ".exe" ) ) {
        return QIcon::fromTheme( "wine" );
    }

    /** Check if a theme icon exists called @mAppId */
    if ( QIcon::hasThemeIcon( mAppId ) ) {
        return QIcon::fromTheme( mAppId );
    }

    /** Check if the theme icon is @mAppId, but all lower-case letters */
    else if ( QIcon::hasThemeIcon( mAppId.toLower() ) ) {
        return QIcon::fromTheme( mAppId.toLower() );
    }

    QStringList appDirs = {
        QDir::home().filePath( ".local/share/applications/" ),
        "/usr/local/share/applications/",
        "/usr/share/applications/",
    };

    /**
     * Assume mAppId == desktop-file-name (ideal situation)
     * or mAppId.toLower() == desktop-file-name (cheap fallback)
     */
    QString iconName( "" );

    for ( QString path: appDirs ) {
        /** Get the icon name from desktop (mAppId: as it is) */
        if ( QFile::exists( path + mAppId + ".desktop" ) ) {
            QSettings desktop( path + mAppId + ".desktop", QSettings::IniFormat );
            iconName = desktop.value( "Desktop Entry/Icon" ).toString();
        }

        /** Get the icon name from desktop (mAppId: all lower-case letters) */
        else if ( QFile::exists( path + mAppId.toLower() + ".desktop" ) ) {
            QSettings desktop( path + mAppId.toLower() + ".desktop", QSettings::IniFormat );
            iconName = desktop.value( "Desktop Entry/Icon" ).toString();
        }

        /** No icon specified: try else-where */
        if ( iconName.isEmpty() ) {
            continue;
        }

        /** We got an iconName, and it's in the current theme */
        if ( QIcon::hasThemeIcon( iconName ) ) {
            return QIcon::fromTheme( iconName );
        }

        /** Not a theme icon, but an absolute path */
        else if ( QFile::exists( iconName ) ) {
            return QIcon( iconName );
        }

        /** Not theme icon or absolute path. So check /usr/share/pixmaps/ */
        else {
            iconName = getPixmapIcon( iconName );

            if ( not iconName.isEmpty() ) {
                return QIcon( iconName );
            }
        }
    }

    /* Check all desktop files for @mAppId */
    for ( QString path: appDirs ) {
        QStringList desktops = QDir( path ).entryList( { "*.desktop" } );
        for ( QString dskf: desktops ) {
            QSettings desktop( path + dskf, QSettings::IniFormat );

            QString exec = desktop.value( "Desktop Entry/Exec", "abcd1234/-" ).toString();
            QString name = desktop.value( "Desktop Entry/Name", "abcd1234/-" ).toString();
            QString cls  = desktop.value( "Desktop Entry/StartupWMClass", "abcd1234/-" ).toString();

            QString execPath( std::filesystem::path( exec.toStdString() ).filename().c_str() );

            if ( mAppId.compare( execPath, Qt::CaseInsensitive ) == 0 ) {
                iconName = desktop.value( "Desktop Entry/Icon" ).toString();
            }

            else if ( mAppId.compare( name, Qt::CaseInsensitive ) == 0 ) {
                iconName = desktop.value( "Desktop Entry/Icon" ).toString();
            }

            else if ( mAppId.compare( cls, Qt::CaseInsensitive ) == 0 ) {
                iconName = desktop.value( "Desktop Entry/Icon" ).toString();
            }

            if ( not iconName.isEmpty() ) {
                if ( QIcon::hasThemeIcon( iconName ) ) {
                    return QIcon::fromTheme( iconName );
                }

                else if ( QFile::exists( iconName ) ) {
                    return QIcon( iconName );
                }

                else {
                    iconName = getPixmapIcon( iconName );

                    if ( not iconName.isEmpty() ) {
                        return QIcon( iconName );
                    }
                }
            }
        }
    }

    iconName = getPixmapIcon( iconName );

    if ( not iconName.isEmpty() ) {
        return QIcon( iconName );
    }

    return QIcon();
}
