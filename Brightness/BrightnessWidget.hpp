/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QMap>
#include <QWidget>
#include <desqui/ShellPlugin.hpp>

class QSlider;
class QSvgWidget;
class QBasicTimer;
class QFileSystemWatcher;
class QGraphicsOpacityEffect;

namespace DFL {
    class Login1;
}

class BacklightDevice : public QObject {
    Q_OBJECT;

    public:
        BacklightDevice( QString path = QString() );

        QString name();

        int currentBrightness();
        int maximumBrightness();
        bool setBrightness( int value );

    private:
        QString devPath;
        qreal maxBrightness;

        QFileSystemWatcher *fsw;

    Q_SIGNALS:
        void updateValue( int );
};

class BrightnessWidget : public DesQ::Shell::PluginWidget {
    Q_OBJECT

    public:
        BrightnessWidget( QWidget *parent = 0 );

    private:
        QByteArray svg;
        QGraphicsOpacityEffect *opacity;

        QMap<QString, BacklightDevice *> devices;
        QMap<QString, QSlider *> deviceSliders;
        QMap<QString, QSvgWidget *> displayWidgets;

        QBasicTimer *changeTimer;
        void changeBrightness( QSlider *sl );

        QStringList toBeChanged;

        DFL::Login1 *login1;

    protected:
        void wheelEvent( QWheelEvent *wEvent );
        void timerEvent( QTimerEvent *tEvent );

        void enterEvent( QEvent * );
        void leaveEvent( QEvent * );
};
