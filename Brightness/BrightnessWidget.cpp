/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <unistd.h>

#include <QtWidgets>
#include <QSlider>
#include <QSvgWidget>
#include <QGraphicsOpacityEffect>
#include <QtDBus>

#include "BrightnessWidget.hpp"

#include <desq/Utils.hpp>
#include <DFL/DF5/Login1.hpp>
#include <desqui/ShellPlugin.hpp>

BacklightDevice::BacklightDevice( QString path ) {
    devPath = QString( path );

    if ( not devPath.endsWith( "/" ) ) {
        devPath += "/";
    }

    QFile curFile( devPath + "max_brightness" );

    maxBrightness = 255.0;

    if ( curFile.open( QIODevice::ReadOnly ) ) {
        maxBrightness = curFile.readAll().simplified().toDouble();
    }

    curFile.close();

    fsw = new QFileSystemWatcher();
    fsw->addPath( devPath + "brightness" );

    connect(
        fsw, &QFileSystemWatcher::fileChanged, [ = ] () {
            emit updateValue( currentBrightness() );
        }
    );
}


QString BacklightDevice::name() {
    return QFileInfo( devPath ).fileName();
}


int BacklightDevice::currentBrightness() {
    /* Brightness scaled to 1000 */
    QFile curFile( devPath + "brightness" );

    if ( not curFile.open( QIODevice::ReadOnly ) ) {
        return -1;
    }

    int curBrightness = curFile.readAll().simplified().toInt();

    curFile.close();

    return curBrightness;
}


int BacklightDevice::maximumBrightness() {
    return maxBrightness;
}


BrightnessWidget::BrightnessWidget( QWidget *parent ) : DesQ::Shell::PluginWidget( parent ) {
    /** Login1 Impl */
    login1 = new DFL::Login1( this );

    /** Brightness Timer */
    changeTimer = new QBasicTimer();

    /** Get the SVG Data */
    QFile svgf( ":/monitor.svg" );

    svgf.open( QFile::ReadOnly );
    svg = svgf.readAll();
    svgf.close();

    /** List all the devices */
    QDirIterator it( "/sys/class/backlight/", QDir::Dirs | QDir::NoDotAndDotDot );
    QStringList  backlightDevices;

    while ( it.hasNext() ) {
        backlightDevices << it.next();
    }

    /**
     * Hack for linux 6.1 kernels
     * All the backlight interfaces are unified under ACPI.
     * In addition to intel_backlight/nouveau_bl/amdgpu_bl0/radeon_bl0,
     * we will also have acpi_video0, etc... So if we have acpi_video*,
     * discard all others.
     */
    if ( backlightDevices.filter( "acpi_video" ).count() ) {
        backlightDevices = backlightDevices.filter( "acpi_video" );
    }

    for ( QString devicePath: backlightDevices ) {
        BacklightDevice *dev = new BacklightDevice( devicePath );
        devices[ it.fileName() ] = dev;

        QSlider *sl = new QSlider( Qt::Horizontal );
        sl->setRange( 1, dev->maximumBrightness() );
        sl->setValue( dev->currentBrightness() );

        connect(
            sl, &QSlider::valueChanged, [ = ] ( int ) {
                changeBrightness( sl );
            }
        );

        connect(
            dev, &BacklightDevice::updateValue, [ = ] ( int val ) {
                disconnect( sl, nullptr, nullptr, nullptr );

                sl->setValue( val );

                connect(
                    sl, &QSlider::valueChanged, [ = ] ( int ) {
                        changeBrightness( sl );
                    }
                );
            }
        );

        deviceSliders[ it.fileName() ] = sl;

        QSvgWidget *display = new QSvgWidget( this );
        display->setFixedSize( 24, 24 );
        displayWidgets[ it.fileName() ] = display;
    }

    /** Layout in which the widgets will be placed */
    QGridLayout *lyt = new QGridLayout( this );

    /** If we don't have any device */
    if ( not deviceSliders.count() ) {
        lyt->addWidget( new QLabel( "No backlight devices." ), 0, 0, Qt::AlignCenter );
    }

    /** More than one found */
    else {
        QStringList devices( deviceSliders.keys() );
        for ( int i = 0; i < deviceSliders.count(); i++ ) {
            QString dev( devices.at( i ) );

            displayWidgets.value( dev )->load( QByteArray( svg ) );
            lyt->addWidget( displayWidgets.value( dev ), 2 * i,     0 );
            lyt->addWidget( new QLabel( dev ),           2 * i,     1, Qt::AlignLeft | Qt::AlignVCenter );
            lyt->addWidget( deviceSliders.value( dev ),  2 * i + 1, 0, 1, 2 );
        }
    }

    // lyt->addStretch();

    setLayout( lyt );

    opacity = new QGraphicsOpacityEffect( this );
    opacity->setOpacity( 0.4 );
    setGraphicsEffect( opacity );
}


void BrightnessWidget::changeBrightness( QSlider *sl ) {
    for ( QString dev: deviceSliders.keys() ) {
        if ( deviceSliders.value( dev ) == sl ) {
            if ( not toBeChanged.contains( dev ) ) {
                toBeChanged << dev;
            }
        }
    }

    changeTimer->start( 250, this );
}


void BrightnessWidget::wheelEvent( QWheelEvent *wEvent ) {
    for ( QString dev: deviceSliders.keys() ) {
        deviceSliders.value( dev )->event( wEvent );
    }

    wEvent->accept();
}


void BrightnessWidget::timerEvent( QTimerEvent *tEvent ) {
    if ( tEvent->timerId() == changeTimer->timerId() ) {
        while ( toBeChanged.count() ) {
            QString dev = toBeChanged.takeFirst();
            qreal   pc  = 100 * deviceSliders.value( dev )->value() / deviceSliders.value( dev )->maximum();
            login1->setBrightness( (pc < 1 ? 1 : pc) );

            displayWidgets.value( dev )->load( QByteArray( svg ).replace( QByteArray( "###" ), QByteArray::number( pc / 100.0, 'f', 2 ) ) );
        }

        changeTimer->stop();
    }

    QWidget::timerEvent( tEvent );
}


void BrightnessWidget::enterEvent( QEvent *event ) {
    opacity->setOpacity( 1.0 );
    event->accept();
}


void BrightnessWidget::leaveEvent( QEvent *event ) {
    opacity->setOpacity( 0.4 );
    event->accept();
}
