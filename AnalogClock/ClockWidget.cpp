/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>
#include <desqui/ShellPlugin.hpp>

#include "ClockWidget.hpp"

DigitalClock::DigitalClock( QWidget *parent ) : DesQ::Shell::PluginWidget( parent ) {
    QDateTime dt   = QDateTime::currentDateTime();
    int       size = font().pointSize() * 16.0 / 12.0 * 2.0;

    time = new QLCDNumber( this );
    time->setDigitCount( 5 );
    time->setSegmentStyle( QLCDNumber::Flat );
    time->setFrameStyle( QLCDNumber::NoFrame );
    time->setSizePolicy( QSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding ) );
    time->setMinimumHeight( size * 2 );
    time->display( dt.toString( "hh:mm" ) );

    secs = new QLCDNumber( this );
    secs->setDigitCount( 2 );
    secs->setSegmentStyle( QLCDNumber::Flat );
    secs->setFrameStyle( QLCDNumber::NoFrame );
    secs->setFixedSize( QSize( size, size ) );
    secs->display( dt.toString( "ss" ) );

    ampm = new QLabel();
    ampm->setAlignment( Qt::AlignCenter );
    ampm->setFixedSize( QSize( size, size ) );
    ampm->setFixedSize( QSize( size, size ) );
    ampm->setText( dt.time().hour() > 12 ? "PM" : "AM" );

    date = new QLabel();
    date->setAlignment( Qt::AlignCenter );
    date->setText( dt.toString( "ddd, MMM dd, yyyy" ) );

    alrm = new QLabel();
    alrm->setAlignment( Qt::AlignCenter );
    alrm->setText( QString::fromUtf8( "\xF0\x9F\x94\x94" ) );

    QGridLayout *lyt = new QGridLayout();

    lyt->addWidget( time, 0, 0, 2, 1 );
    lyt->addWidget( secs, 0, 1, Qt::AlignCenter );
    lyt->addWidget( ampm, 1, 1, Qt::AlignCenter );
    lyt->addWidget( date, 2, 0, Qt::AlignCenter );
    lyt->addWidget( alrm, 2, 1, Qt::AlignCenter );
    setLayout( lyt );

    setFixedHeight( size * 3.5 );

    QGraphicsDropShadowEffect *shadow = new QGraphicsDropShadowEffect();

    shadow->setOffset( 1, 1 );
    shadow->setColor( palette().color( QPalette::Window ) );
    setGraphicsEffect( shadow );

    timer = new QBasicTimer();
    timer->start( 1000, this );
}


void DigitalClock::timerEvent( QTimerEvent *event ) {
    if ( event->timerId() == timer->timerId() ) {
        QDateTime dt = QDateTime::currentDateTime();
        time->display( dt.toString( "hh:mm" ) );
        secs->display( dt.toString( "ss" ) );
        ampm->setText( dt.time().hour() > 12 ? "PM" : "AM" );
        date->setText( dt.toString( "ddd, MMM dd, yyyy" ) );
    }

    QWidget::timerEvent( event );
}
