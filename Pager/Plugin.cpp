/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "Plugin.hpp"
#include "PagerWidget.hpp"

/* Name of the plugin */
QString PagerPlugin::name() {
    return "Pager";
}


/* Name of the plugin */
QIcon PagerPlugin::icon() {
    return QIcon::fromTheme( "desq" );
}


/* The plugin version */
QString PagerPlugin::version() {
    return QString( "1.0.0" );
}


/* The Pager plugin */
DesQ::Shell::PluginWidget *PagerPlugin::widget( QWidget *parent ) {
    return new PagerWidget( parent );
}
