/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include <QtWidgets>
#include <QtDBus>
#include "Global.hpp"
#include "PagerWidget.hpp"

#include <wayqt/WayQtUtils.hpp>
#include <wayqt/Registry.hpp>
#include <wayqt/DesQShell.hpp>
#include <desqui/ShellPlugin.hpp>

WorkspaceWidget::WorkspaceWidget( WorkSpace ws, int number, QWidget *parent ) : QWidget( parent ) {
    mCurWS  = ws;
    mNumber = number;
    setFixedSize( QSize( 32, 24 ) );
}


WorkSpace WorkspaceWidget::workSpace() {
    return mCurWS;
}


void WorkspaceWidget::highlight( bool yes ) {
    mHighlight = yes;
    repaint();
}


void WorkspaceWidget::mousePressEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        mPressed = true;
    }

    mEvent->accept();
}


void WorkspaceWidget::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( (mEvent->button() == Qt::LeftButton) and mPressed ) {
        emit switchWorkSpace( mCurWS );
    }

    mEvent->accept();
}


void WorkspaceWidget::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHint( QPainter::Antialiasing );

    painter.save();

    if ( mHighlight ) {
        QColor clr( palette().color( QPalette::Highlight ) );
        clr = clr.darker();
        painter.setPen( QPen( clr, 1.0 ) );
        clr = clr.lighter();
        clr.setAlphaF( 0.5 );
        painter.setBrush( clr );
    }

    else {
        QColor clr( palette().color( QPalette::Window ) );
        painter.setPen( QPen( clr, 1.0 ) );
        clr.setAlphaF( 0.5 );
        painter.setBrush( clr );
    }

    painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 3, 3 );
    painter.restore();

    painter.save();

    if ( mHighlight ) {
        QFont f( font() );
        f.setWeight( QFont::Bold );
        painter.setFont( f );
    }

    painter.drawText( rect(), Qt::AlignCenter, QString::number( mNumber ) );
    painter.restore();

    painter.end();

    pEvent->accept();
}


PagerWidget::PagerWidget( QWidget *parent ) : DesQ::Shell::PluginWidget( parent ) {
    qRegisterMetaType<QUIntList>( "QUIntList" );
    qDBusRegisterMetaType<QUIntList>();

    qRegisterMetaType<WorkSpace>( "WorkSpace" );
    qDBusRegisterMetaType<WorkSpace>();

    qRegisterMetaType<WorkSpaces>( "WorkSpaces" );
    qDBusRegisterMetaType<WorkSpaces>();

    qRegisterMetaType<WorkSpaceGrid>( "WorkSpaceGrid" );
    qDBusRegisterMetaType<WorkSpaceGrid>();

    wsLyt = new QGridLayout();
    setLayout( wsLyt );

    connect(
        this, &DesQ::Shell::PluginWidget::widgetScreenChanged, [ = ] ( QScreen *scrn ) {
            WQt::DesQShell *shell = nullptr;

            int time = 0;
            do {
                /** Sleep for 10 ms */
                QThread::msleep( 10 );
                time += 10;

                /** Process outstanding queue */
                qApp->processEvents();

                /** Attempt to retrieve DesQShell interface */
                shell = wlRegistry->desqShell();

                if ( time > 5000 ) {
                    qDebug() << "Unable to bind to DesQ Shell interface. Please ensure that the compositor supports it.";
                    break;
                }
            } while ( shell == nullptr );

            if ( shell ) {
                mDesQOp = shell->getOutput( WQt::Utils::wlOutputFromQScreen( scrn ) );

                if ( mDesQOp ) {
                    /** Store the incoming workspace grid in pending state */
                    connect(
                        mDesQOp, &WQt::DesQOutput::workspaceExtentsChanged, [ = ] ( int rows, int cols ) {
                            pendingGrid.rows    = rows;
                            pendingGrid.columns = cols;
                        }
                    );

                    /** Store the incoming workspace grid in pending state */
                    connect(
                        mDesQOp, &WQt::DesQOutput::activeWorkspaceChanged, [ = ] ( int row, int col ) {
                            pendingWS.row    = row;
                            pendingWS.column = col;
                        }
                    );

                    /** Commit our pending states */
                    connect(
                        mDesQOp, &WQt::DesQOutput::done, [ = ] () mutable {
                            if ( pendingGrid.rows and pendingGrid.columns ) {
                                populateLayout( pendingGrid );
                                pendingGrid.rows    = 0;
                                pendingGrid.columns = 0;
                            }

                            if ( pendingWS.row >= 0 and pendingWS.column >= 0 ) {
                                highlightWorkspace( pendingWS );
                                pendingWS.row    = -1;
                                pendingWS.column = -1;
                            }
                        }
                    );

                    mDesQOp->setup();
                }
            }
        }
    );

    /** So that we know exactly where the mouse is */
    setMouseTracking( true );
}


void PagerWidget::populateLayout( WorkSpaceGrid grid ) {
    /* Delete the previously added ClipboardItem */
    if ( wsLyt->count() ) {
        qDeleteAll( wsLyt->findChildren<WorkspaceWidget *>() );
    }

    if ( workspaceList.count() ) {
        qDeleteAll( workspaceList );
    }

    workspaceList.clear();

    for ( int r = 0; r < grid.rows; r++ ) {
        for ( int c = 0; c < grid.columns; c++ ) {
            int             wsCount = r * grid.columns + c + 1;
            WorkspaceWidget *wsw    = new WorkspaceWidget( { r, c }, wsCount, this );
            wsLyt->addWidget( wsw, r, c );
            workspaceList << wsw;

            connect(
                wsw, &WorkspaceWidget::switchWorkSpace, [ = ] ( WorkSpace ws ) {
                    mDesQOp->setActiveWorkspace( ws.row, ws.column );
                }
            );
        }
    }
}


void PagerWidget::highlightWorkspace( WorkSpace ws ) {
    qDebug() << "Attempting to highlight:" << ws.row << "x" << ws.column;
    for ( WorkspaceWidget *wsw: workspaceList ) {
        if ( wsw->workSpace() == ws ) {
            wsw->highlight( true );
        }

        else {
            wsw->highlight( false );
        }
    }
}
