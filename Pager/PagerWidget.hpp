/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QWidget>
#include <QGridLayout>
#include <desqui/ShellPlugin.hpp>

#include "dbustypes.hpp"

class QDBusInterface;

namespace WQt {
    class DesQOutput;
}

class WorkspaceWidget : public QWidget {
    Q_OBJECT;

    public:
        WorkspaceWidget( WorkSpace, int number, QWidget * );

        WorkSpace workSpace();

        void highlight( bool );

    private:
        WorkSpace mCurWS;
        int mNumber;
        bool mPressed   = false;
        bool mHighlight = false;

    protected:
        void mousePressEvent( QMouseEvent *mEvent );
        void mouseReleaseEvent( QMouseEvent *mEvent );

        void paintEvent( QPaintEvent *pEvent );

    Q_SIGNALS:
        void switchWorkSpace( WorkSpace );
};

class PagerWidget : public DesQ::Shell::PluginWidget {
    Q_OBJECT

    public:
        PagerWidget( QWidget *parent = 0 );

    private:
        QGridLayout *wsLyt;
        WQt::DesQOutput *mDesQOp;

        QList<WorkspaceWidget *> workspaceList;

        WorkSpaceGrid pendingGrid;
        WorkSpace pendingWS;

        /** Populate the layout */
        Q_SLOT void populateLayout( WorkSpaceGrid );

        /** Highlight the active workspace */
        Q_SLOT void highlightWorkspace( WorkSpace );
};
