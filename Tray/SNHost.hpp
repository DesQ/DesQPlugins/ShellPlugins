/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DesQ
 * Any and all bug reports are to be filed with DesQ and not LXQt.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QDir>
#include <QDebug>
#include <QWidget>
#include <QHBoxLayout>
#include <QPushButton>

#include "SNButton.hpp"
#include <desqui/PanelPlugin.hpp>

class TrayPluginWidget : public DesQ::Panel::PluginWidget {
    Q_OBJECT

    public:
        enum LayoutStyle {
            Horizontal = 0x458651,
            Vertical,
            Grid,
        };

        TrayPluginWidget( QWidget *parent = nullptr );
        ~TrayPluginWidget();

        Q_SLOT void itemAdded( QString serviceAndPath );
        Q_SLOT void itemRemoved( QString serviceAndPath );

    private:
        QDBusInterface *mWatcher;
        QHash<QString, StatusNotifierButton *> mServices;

        QHBoxLayout *sniLyt;
        LayoutStyle mLytStyle = Horizontal;

        int mItemSpacing = 0;
        QMargins mMargins{ 0, 0, 0, 0 };

        /* Resize the buttons and the widget */
        void resizeButton( StatusNotifierButton *btn );
};


class TrayPlugin : public QObject, public DesQ::Plugin::PanelInterface {
    Q_OBJECT

    Q_PLUGIN_METADATA( IID "org.DesQ.Plugin.Panel" );
    Q_INTERFACES( DesQ::Plugin::PanelInterface );

    public:
        /* Name of the plugin */
        QString name() override {
            return "System Tray";
        }

        /* Icon for the plugin */
        QIcon icon() override {
            return QIcon::fromTheme( "org.kde.plasma.systemtray" );
        }

        /* The plugin version */
        QString version() override {
            return "0.0.8";
        }

        /* The clock Widget */
        DesQ::Panel::PluginWidget *widget( QWidget *parent ) override {
            return new TrayPluginWidget( parent );
        }
};
