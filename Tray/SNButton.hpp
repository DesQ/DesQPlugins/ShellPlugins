/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DesQ
 * Any and all bug reports are to be filed with DesQ and not LXQt.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QDBusArgument>
#include <QDBusMessage>
#include <QDBusInterface>
#include <QMouseEvent>
#include <QToolButton>
#include <QWheelEvent>
#include <QLabel>
#include <QMenu>

#if QT_VERSION < QT_VERSION_CHECK( 5, 5, 0 )
template<typename T> inline T qFromUnaligned( const uchar *src ) {
    T            dest;
    const size_t size = sizeof(T);

    memcpy( &dest, src, size );
    return dest;
}


#endif

namespace DFL {
    class StatusNotifierItem;
}

class StatusNotifierButton : public QToolButton {
    Q_OBJECT;

    public:
        StatusNotifierButton( QString service, QString objectPath, QWidget *parent = nullptr );
        ~StatusNotifierButton();

        enum Status {
            Passive, Active, NeedsAttention
        };

    public slots:
        void newIcon();
        void newAttentionIcon();
        void newOverlayIcon();
        void newToolTip();
        void newStatus( QString status );

    private:
        void checkCanActivate( QString service, QString path );

        DFL::StatusNotifierItem *interface;
        QMenu *mMenu;
        Status mStatus;

        QIcon mIcon, mOverlayIcon, mAttentionIcon, mFallbackIcon;
        QLabel *mTooltip = nullptr;

        /**
         * If Activate method exists: ex Element/Signal dont have it
         * When apps don't have `Activate`, we show the menu
         */
        bool mCanActivate = false;

    protected:
        void contextMenuEvent( QContextMenuEvent *event );
        void mouseReleaseEvent( QMouseEvent *event );
        void wheelEvent( QWheelEvent *event );

        void enterEvent( QEvent *event );
        void leaveEvent( QEvent *event );

        void refetchIcon( Status status, const QString& themePath );
        void resetIcon();

    Q_SIGNALS:
        void showPopup( QWidget * );
        void showTooltip( QWidget * );
};
