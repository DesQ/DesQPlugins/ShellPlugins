/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DesQ
 * Any and all bug reports are to be filed with DesQ and not LXQt.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "SNButton.hpp"

#include <QDir>
#include <QFile>
#include <dbusmenu-qt5/dbusmenuimporter.h>

#include <DFL/DF5/StatusNotifierItem.hpp>
#include <DFL/DF5/SNITypes.hpp>

namespace {
    class MenuImporter : public DBusMenuImporter {
        public:
            using DBusMenuImporter::DBusMenuImporter;

        protected:
            virtual QIcon iconForName( const QString& name ) override {
                return QIcon::fromTheme( name );
            }
    };
}


StatusNotifierButton::StatusNotifierButton( QString service, QString objectPath, QWidget *parent ) : QToolButton( parent ) {
    mMenu   = nullptr;
    mStatus = Passive;

    setFixedSize( QSize( 28, 28 ) );
    setIconSize( QSize( 22, 22 ) );
    setAutoRaise( true );
    setFocusPolicy( Qt::NoFocus );

    mFallbackIcon = QIcon::fromTheme( "application-x-executable" );

    mTooltip = new QLabel();
    mTooltip->setWindowFlags( Qt::Widget | Qt::BypassWindowManagerHint );
    mTooltip->setStyleSheet( "padding: 5px; border: 1px solid white; border-radius: 3px; background-color: rgba(0, 0, 0, 0.9);" );

    interface = new DFL::StatusNotifierItem( service, objectPath, QDBusConnection::sessionBus(), this );
    checkCanActivate( service, objectPath );

    connect( interface, &DFL::StatusNotifierItem::NewIcon,          this, &StatusNotifierButton::newIcon );
    connect( interface, &DFL::StatusNotifierItem::NewOverlayIcon,   this, &StatusNotifierButton::newOverlayIcon );
    connect( interface, &DFL::StatusNotifierItem::NewAttentionIcon, this, &StatusNotifierButton::newAttentionIcon );
    connect( interface, &DFL::StatusNotifierItem::NewToolTip,       this, &StatusNotifierButton::newToolTip );
    connect( interface, &DFL::StatusNotifierItem::NewStatus,        this, &StatusNotifierButton::newStatus );

    interface->propertyGetAsync(
        QLatin1String( "Menu" ), [ this ] ( QDBusObjectPath path ) {
            if ( not path.path().isEmpty() ) {
                mMenu = (new MenuImporter { interface->service(), path.path(), ( QWidget * )this->parent() })->menu();
                mMenu->setObjectName( QLatin1String( "StatusNotifierMenu" ) );
            }
        }
    );

    interface->propertyGetAsync(
        QLatin1String( "Status" ), [ this ]( QString status ) {
            newStatus( status );
        }
    );

    interface->propertyGetAsync(
        QLatin1String( "IconThemePath" ), [ this ]( QString value ) {
            refetchIcon( Active,         value );
            refetchIcon( Passive,        value );
            refetchIcon( NeedsAttention, value );
        }
    );

    newToolTip();
}


StatusNotifierButton::~StatusNotifierButton() {
    delete interface;
}


void StatusNotifierButton::checkCanActivate( QString service, QString objPath ) {
    QDBusInterface             iface( service, objPath, "org.freedesktop.DBus.Introspectable", QDBusConnection::sessionBus() );
    QDBusPendingReply<QString> reply = iface.call( "Introspect" );

    if ( reply.isError() ) {
        qWarning() << "Failed to fetch tray icon methods";
        return;
    }

    QXmlStreamReader xml( reply.value() );

    if ( xml.hasError() ) {
        qWarning() << "Failed to parse tray icon XML.";
        return;
    }

    QList<QDBusObjectPath> objects;
    bool ourInterface = false;

    while ( !xml.atEnd() ) {
        xml.readNext();

        /* We're bothered only about org.kde.StatusNotifierItem */
        if ( (xml.tokenType() == QXmlStreamReader::StartElement) && (xml.name().toString() == "interface") ) {
            QString name = xml.attributes().value( "name" ).toString();

            if ( name == "org.kde.StatusNotifierItem" ) {
                ourInterface = true;
            }
        }

        if ( ourInterface ) {
            if ( (xml.tokenType() == QXmlStreamReader::StartElement) && (xml.name().toString() == "method") ) {
                QString name = xml.attributes().value( "name" ).toString();

                if ( name == "Activate" ) {
                    mCanActivate = true;
                    break;
                }
            }
        }
    }
}


void StatusNotifierButton::newIcon() {
    interface->propertyGetAsync(
        QLatin1String( "IconThemePath" ), [ this ] ( QString value ) {
            refetchIcon( Passive, value );
        }
    );
}


void StatusNotifierButton::newOverlayIcon() {
    interface->propertyGetAsync(
        QLatin1String( "IconThemePath" ), [ this ] ( QString value ) {
            refetchIcon( Active, value );
        }
    );
}


void StatusNotifierButton::newAttentionIcon() {
    interface->propertyGetAsync(
        QLatin1String( "IconThemePath" ), [ this ] ( QString value ) {
            refetchIcon( NeedsAttention, value );
        }
    );
}


void StatusNotifierButton::refetchIcon( Status status, const QString& themePath ) {
    QString nameProperty, pixmapProperty;

    if ( status == Active ) {
        nameProperty   = QLatin1String( "OverlayIconName" );
        pixmapProperty = QLatin1String( "OverlayIconPixmap" );
    }

    else if ( status == NeedsAttention ) {
        nameProperty   = QLatin1String( "AttentionIconName" );
        pixmapProperty = QLatin1String( "AttentionIconPixmap" );
    }

    else { // status == Passive
        nameProperty   = QLatin1String( "IconName" );
        pixmapProperty = QLatin1String( "IconPixmap" );
    }

    interface->propertyGetAsync(
        nameProperty, [ this, status, pixmapProperty, themePath ] ( QString iconName ) {
            QIcon nextIcon;

            if ( not iconName.isEmpty() ) {
                if ( QIcon::hasThemeIcon( iconName ) ) {
                    nextIcon = QIcon::fromTheme( iconName );
                }

                else {
                    QDir themeDir( themePath );

                    if ( themeDir.exists() ) {
                        if ( themeDir.exists( iconName + ".png" ) ) {
                            nextIcon.addFile( themeDir.filePath( iconName + ".png" ) );
                        }

                        if ( themeDir.cd( "hicolor" ) || (themeDir.cd( "icons" ) && themeDir.cd( "hicolor" ) ) ) {
                            const QStringList sizes = themeDir.entryList( QDir::AllDirs | QDir::NoDotAndDotDot );

                            for ( const QString& dir : sizes ) {
                                const QStringList dirs = QDir( themeDir.filePath( dir ) ).entryList( QDir::AllDirs | QDir::NoDotAndDotDot );
                                for ( const QString& innerDir : dirs ) {
                                    QString file = themeDir.absolutePath() + "/" + dir + "/" + innerDir + "/" + iconName + ".png";

                                    if ( QFile::exists( file ) ) {
                                        nextIcon.addFile( file );
                                    }
                                }
                            }
                        }
                    }
                }

                switch ( status ) {
                        case Active: {
                            mOverlayIcon = nextIcon;
                            break;
                        }

                        case NeedsAttention: {
                            mAttentionIcon = nextIcon;
                            break;
                        }

                        case Passive: {
                            mIcon = nextIcon;
                            break;
                        }
                }

                resetIcon();
            }

            else {
                interface->propertyGetAsync(
                    pixmapProperty, [ this, status, pixmapProperty ] ( DFL::SNI::IconPixmapList iconPixmaps ) {
                        if ( iconPixmaps.empty() ) {
                            return;
                        }

                        QIcon nextIcon;

                        for ( DFL::SNI::IconPixmap iconPixmap: iconPixmaps ) {
                            if ( not iconPixmap.bytes.isNull() ) {
                                QImage image( ( uchar * )iconPixmap.bytes.data(), iconPixmap.width,
                                              iconPixmap.height, QImage::Format_ARGB32 );

                                const uchar *end = image.constBits() + image.sizeInBytes();
                                uchar *dest      = reinterpret_cast<uchar *>(iconPixmap.bytes.data() );
                                for ( const uchar *src = image.constBits(); src < end; src += 4, dest += 4 ) {
                                    qToUnaligned( qToBigEndian<quint32>( qFromUnaligned<quint32>( src ) ), dest );
                                }

                                nextIcon.addPixmap( QPixmap::fromImage( image ) );
                            }
                        }

                        switch ( status ) {
                                case Active: {
                                    mOverlayIcon = nextIcon;
                                    break;
                                }

                                case NeedsAttention: {
                                    mAttentionIcon = nextIcon;
                                    break;
                                }

                                case Passive: {
                                    mIcon = nextIcon;
                                    break;
                                }
                        }

                        resetIcon();
                    }
                );
            }
        }
    );
}


void StatusNotifierButton::newToolTip() {
    interface->propertyGetAsync(
        QLatin1String( "ToolTip" ), [ this ]( DFL::SNI::ToolTip tooltip ) {
            QString toolTipTitle = tooltip.title;
            QString toolTipDescr = tooltip.description;

            if ( toolTipTitle.length() ) {
                if ( toolTipDescr.length() ) {
                    mTooltip->setText( QString( "<b>%1</b><p>%2</p>" ).arg( toolTipTitle ).arg( toolTipDescr ) );
                }

                else {
                    mTooltip->setText( toolTipTitle );
                }
            }

            else {
                interface->propertyGetAsync(
                    QLatin1String( "Title" ), [ this ]( QString title ) {
                        if ( title.length() ) {
                            mTooltip->setText( title );
                        }
                    }
                );
            }
        }
    );
}


void StatusNotifierButton::newStatus( QString status ) {
    Status newStatus;

    if ( status == QLatin1String( "Passive" ) ) {
        newStatus = Passive;
    }

    else if ( status == QLatin1String( "Active" ) ) {
        newStatus = Active;
    }

    else {
        newStatus = NeedsAttention;
    }

    if ( mStatus == newStatus ) {
        return;
    }

    mStatus = newStatus;
    resetIcon();
}


void StatusNotifierButton::contextMenuEvent( QContextMenuEvent *event ) {
    if ( mMenu and mMenu->isVisible() ) {
        mMenu->close();
    }

    if ( mMenu and mMenu->actions().length() ) {
        mMenu->setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );

        for ( QAction *act: mMenu->actions() ) {
            if ( act->menu() ) {
                act->menu()->setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );
            }

            else {
                QObject::connect(
                    act, &QAction::triggered, [ = ] ( bool ) {
                        if ( mMenu->isVisible() ) {
                            mMenu->close();
                        }
                    }
                );
            }
        }

        emit showPopup( mMenu );
    }

    else {
        qWarning() << "Context Menu event";
        interface->ContextMenu( QCursor::pos().x(), QCursor::pos().y() );
    }
}


void StatusNotifierButton::mouseReleaseEvent( QMouseEvent *event ) {
    if ( event->button() == Qt::LeftButton ) {
        if ( mCanActivate ) {
            interface->Activate( QCursor::pos().x(), QCursor::pos().y() );
        }

        else {
            if ( mMenu and mMenu->isVisible() ) {
                mMenu->close();
            }

            if ( mMenu and mMenu->actions().length() ) {
                mMenu->setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );

                for ( QAction *act: mMenu->actions() ) {
                    if ( act->menu() ) {
                        act->menu()->setWindowFlags( Qt::Widget | Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );
                    }

                    else {
                        QObject::connect(
                            act, &QAction::triggered, [ = ] ( bool ) {
                                if ( mMenu->isVisible() ) {
                                    mMenu->close();
                                }
                            }
                        );
                    }
                }

                emit showPopup( mMenu );
            }

            else {
                qWarning() << "Context Menu event";
                interface->ContextMenu( QCursor::pos().x(), QCursor::pos().y() );
            }
        }
    }

    else if ( event->button() == Qt::MiddleButton ) {
        interface->SecondaryActivate( QCursor::pos().x(), QCursor::pos().y() );
    }

    else if ( Qt::RightButton == event->button() ) {
        // Handled by context menu event
    }

    QToolButton::mouseReleaseEvent( event );
}


void StatusNotifierButton::enterEvent( QEvent *event ) {
    if ( mTooltip->isVisible() ) {
        return;
    }

    if ( mTooltip->text().length() ) {
        emit showTooltip( mTooltip );
    }
}


void StatusNotifierButton::leaveEvent( QEvent *event ) {
    if ( mTooltip->isVisible() ) {
        mTooltip->close();
    }
}


void StatusNotifierButton::wheelEvent( QWheelEvent *event ) {
    interface->Scroll( event->angleDelta().y(), "vertical" );
}


void StatusNotifierButton::resetIcon() {
    if ( (mStatus == Active) && not mOverlayIcon.isNull() ) {
        setIcon( mOverlayIcon );
    }

    else if ( (mStatus == NeedsAttention) && not mAttentionIcon.isNull() ) {
        setIcon( mAttentionIcon );
    }

    else if ( not mIcon.isNull() ) { // mStatus == Passive
        setIcon( mIcon );
    }

    else if ( not mOverlayIcon.isNull() ) {
        setIcon( mOverlayIcon );
    }

    else if ( not mAttentionIcon.isNull() ) {
        setIcon( mAttentionIcon );
    }

    else {
        setIcon( mFallbackIcon );
    }
}
