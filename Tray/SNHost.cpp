/**
 * Copyright 2019-2023 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DesQ
 * Any and all bug reports are to be filed with DesQ and not LXQt.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "SNHost.hpp"
#include <QDebug>
#include <QtDBus>
#include <QLabel>

#define WATCHER_SERVICE    "org.kde.StatusNotifierWatcher"
#define WATCHER_OBJECT     "/StatusNotifierWatcher"
#define WATCHER_PATH       "org.kde.StatusNotifierWatcher"

TrayPluginWidget::TrayPluginWidget( QWidget *parent ) : DesQ::Panel::PluginWidget( parent ) {
    QString dbusName = QString( "org.kde.StatusNotifierHost-%1-%2" ).arg( qApp->applicationPid() ).arg( 1 );

    mWatcher = new QDBusInterface( WATCHER_SERVICE, WATCHER_OBJECT, WATCHER_PATH, QDBusConnection::sessionBus() );

    if ( mWatcher->isValid() ) {
        QDBusReply<void> reply = mWatcher->call( "RegisterStatusNotifierHost", dbusName );

        if ( reply.isValid() ) {
            qInfo() << "Registered this widget as StatusNotifierHost.";
        }

        else {
            qDebug() << "Failed to register this widget as StatusNotifierHost. Clients may not realize that the tray is running.";
        }
    }

    else {
        qWarning() << "Unable to connect to " WATCHER_SERVICE;
    }

    sniLyt = new QHBoxLayout();
    sniLyt->setAlignment( Qt::AlignLeft );
    sniLyt->setSpacing( mItemSpacing );
    sniLyt->setContentsMargins( mMargins );
    setLayout( sniLyt );

    QDBusConnection::sessionBus().connect(
        WATCHER_SERVICE,
        WATCHER_OBJECT,
        WATCHER_PATH,
        "StatusNotifierItemRegistered",
        "s",
        this,
        SLOT( itemAdded( QString ) )
    );

    QDBusConnection::sessionBus().connect(
        WATCHER_SERVICE,
        WATCHER_OBJECT,
        WATCHER_PATH,
        "StatusNotifierItemUnregistered",
        "s",
        this,
        SLOT( itemRemoved( QString ) )
    );

    QVariant registered = mWatcher->property( "RegisteredStatusNotifierItems" );

    for ( QString sni: registered.toStringList() ) {
        itemAdded( sni );
    }
}


TrayPluginWidget::~TrayPluginWidget() {
    delete mWatcher;
}


void TrayPluginWidget::itemAdded( QString serviceAndPath ) {
    if ( mServices.contains( serviceAndPath ) ) {
        return;
    }

    int                  slash   = serviceAndPath.indexOf( '/' );
    QString              serv    = serviceAndPath.left( slash );
    QString              path    = serviceAndPath.mid( slash );
    StatusNotifierButton *button = new StatusNotifierButton( serv, path, this );

    resizeButton( button );

    mServices.insert( serviceAndPath, button );
    sniLyt->addWidget( button );

    connect( button, &StatusNotifierButton::showPopup,   this, &DesQ::Panel::PluginWidget::showPopup );
    connect( button, &StatusNotifierButton::showTooltip, this, &DesQ::Panel::PluginWidget::showTooltip );
}


void TrayPluginWidget::itemRemoved( QString serviceAndPath ) {
    StatusNotifierButton *button = mServices.value( serviceAndPath, nullptr );

    if ( button ) {
        button->deleteLater();
        sniLyt->removeWidget( button );

        mServices.remove( serviceAndPath );
    }
}


void TrayPluginWidget::resizeButton( StatusNotifierButton *btn ) {
    btn->setFixedSize( panelHeight, panelHeight );
    btn->setIconSize( QSize( panelHeight - 4, panelHeight - 4 ) );
}
