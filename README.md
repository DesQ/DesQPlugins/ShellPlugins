# Shell Plugins
These are a few simply plugins for DesQ Shell, i.e. to be used with DesQ Cask and DesQ Panel.
Not all plugins are usable with both components. Currently, we have the following plugins

* AnalogClock - QML Analog Clock for Cask
* Battery - Battery plugin for Panel, with brightness control
* Brightness - Brightness control fro Cask
* Clock - Digital clock for Cask and Panel
* Menu - Menu button for Panel
* Pager - Workspace switcher plugin for Cask and Panel
* Power - Power buttons plugin for Cask and Panel
* Resources - Resources plugin for Cask
* Tasks - Tasks plugin for Tasks
* Tray - SNI Tray plugin for Cask and Panel
* Volume - Volume plugin for Cask and Panel
* Weather - Weather plugin for Cask and Panel
