/**
 * Copyright 2019-2021 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file is a part of DesQ Shell (https://gitlab.com/DesQ/Shell)
 * DesQ Shell is the main Shell UI of the DesQ Project
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include <QWidget>
#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QBasicTimer>
#include <QToolButton>
#include <QListWidget>
#include <QGraphicsOpacityEffect>
#include <desqui/ShellPlugin.hpp>

#include <DFL/DF5/Settings.hpp>

class WeatherWidget : public DesQ::Shell::PluginWidget {
    Q_OBJECT

    public:
        WeatherWidget( QWidget *parent = 0 );

    private:
        QLabel *icon;
        QLabel *place;
        QLabel *temp;
        QLabel *tempRange;

        /** Update the weather information hourly */
        QBasicTimer *hourlyTimer;

        /** Timer to delay the information gathering */
        QTimer *delayTimer;

        QGraphicsOpacityEffect *opacity;

        DFL::Settings *weatherSett;

        bool placeInit = false;

        void getCurrentWeatherInfo();
        bool getPlaceInfo();

    protected:
        void mousePressEvent( QMouseEvent *mEvent );
        void timerEvent( QTimerEvent *tEvent );

        void enterEvent( QEvent * );
        void leaveEvent( QEvent * );
};


class LocationDialog : public QDialog {
    Q_OBJECT;

    public:
        LocationDialog();

        QVariantList location();

    private:
        QLabel *text;
        QLineEdit *locationLE;
        QListWidget *results;

        QToolButton *searchBtn;
        QToolButton *selectBtn;

        void searchLocation();

        QVariantList selectedLocation;
};
